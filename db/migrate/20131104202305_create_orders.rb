class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :Menu
      t.references :Ticket

      t.timestamps
    end
    add_index :orders, :Menu_id
    add_index :orders, :Ticket_id
  end
end
