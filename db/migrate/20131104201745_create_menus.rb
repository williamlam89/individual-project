class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :MenuItem
      t.float :MenuPrice

      t.timestamps
    end
  end
end
