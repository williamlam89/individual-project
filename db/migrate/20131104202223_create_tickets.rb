class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :CustomerName
      t.string :TicketStatus
      t.float :TicketTotal

      t.timestamps
    end
  end
end
