class Order < ActiveRecord::Base

  belongs_to :Menu
  belongs_to :Ticket
   attr_accessible :title, :body
end
