class Menu < ActiveRecord::Base
  attr_accessible :MenuItem, :MenuPrice
  has_many :orders
  has_many :tickets, through: :orders
  belongs_to :ticket
end
