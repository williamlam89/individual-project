class Ticket < ActiveRecord::Base
  attr_accessible :CustomerName, :TicketStatus, :TicketTotal, :quantity
  has_many :orders
  has_many :orders, through: :tickets
  belongs_to :menu
end
